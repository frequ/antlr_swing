package tb.antlr;

public class ZeroDivisionException extends RuntimeException {
	public ZeroDivisionException() {
		super("Division by zero is not allowed.");
	}
}
