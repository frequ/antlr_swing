tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr)* ;

expr returns [Integer out]
	      : ID                       {$out = getVariable($ID.text);}
	      | ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = substract($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = multiply($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = powerTo($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = getModulo($e1.out, $e2.out);}
        | ^(VAR   name=ID)         {createVariable($name.text);}
        | ^(EQ    e1=expr e2=expr) {$out = $e1.out == $e2.out ? 1:0;}
        | ^(NE    e1=expr e2=expr) {$out = $e1.out != $e2.out ? 1:0;}
        | ^(GT    e1=expr e2=expr) {$out = $e1.out > $e2.out ? 1:0;}
        | ^(LT    e1=expr e2=expr) {$out = $e1.out < $e2.out ? 1:0;}
        | ^(LE    e1=expr e2=expr) {$out = $e1.out <= $e2.out ? 1:0;}
        | ^(GE    e1=expr e2=expr) {$out = $e1.out >= $e2.out ? 1:0;}
        | ^(PRINT e1=expr)         {printObject($e1.out);}
        | ^(PODST name=ID e1=expr) {setVariable($name.text, $e1.out);}
        | INT                      {$out = getInt($INT.text);} 
        ;

catch [RuntimeException exception] {
  printException(exception);
}