package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.ZeroDivisionException;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	protected LocalSymbols localSymbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }
    
    /**
     * Adding two numbers
     * @param firstNumber: integer
     * @param secondNumber: integer
     * @return sum of two given arguments: integer
     */
    protected Integer add(Integer firstNumber, Integer secondNumber) {
    	return firstNumber + secondNumber;
    }
    
    /**
     * Subtracting two numbers
     * @param firstNumber: integer
     * @param secondNumber: integer
     * @return subtraction of two given arguments: integer
     */
    protected Integer substract(Integer firstNumber, Integer secondNumber) {
    	return firstNumber - secondNumber;
    }
    
    /**
     * Multiplying two numbers
     * @param firstNumber
     * @param secondNumber
     * @return multiplication of two given arguments: integer
     */
    protected Integer multiply(Integer firstNumber, Integer secondNumber) {
    	return firstNumber * secondNumber;
    }
    
    /**
     * Divides two numbers and throws exception if other number equals zero
     * 
     * @param firstNumber: integer
     * @param secondNumber: integer
     * @return result of equation: integer
     */
    protected Integer divide(Integer firstNumber, Integer secondNumber) throws ZeroDivisionException {
    	if(secondNumber == 0)
    		throw new ZeroDivisionException();
    	
    	return firstNumber / secondNumber;
    }
    
    /**
     * Expanding a given number to a given power
     * @param number: integer
     * @param power: integer
     * @return exponentiation result of two given arguments: integer
     */
    protected Integer powerTo(Integer number, Integer power) {
    	return (int) Math.pow(number, power);
    }
    
    /**
     * Returns modulo of two given arguments
     * @param firstNumber: integer
     * @param secondNumber: integer
     * @return result of modulo operation: integer
     */
    protected Integer getModulo(Integer firstNumber, Integer secondNumber) {
    	return firstNumber % secondNumber;
    }
    
    /**
     * Print object given as an argument
     * @param printObject: Object
     */
    protected void printObject(Object printObject) {
    	try {
    		System.out.println(printObject.toString());
    	} catch(Exception exception) {
    		System.err.println(exception.getMessage().toString());
    	}
    }
    
    /**
     * Prints RuntimeException message
     * @param exception
     */
    protected void printException(RuntimeException exception) {
    	System.err.println(exception.getMessage());
    }
    
    /**
     * Creating new variable with given name
     * @param name
     */
    protected void createVariable(String name) {
    	this.localSymbols.newSymbol(name);
    }
    
    /**
     * Setting variable with given name to given value
     * @param name
     * @param value
     */
    protected void setVariable(String name, int value) {
    	this.localSymbols.setSymbol(name, value);
    }
    
    /**
     * Getting variable with given name
     * @param name
     * @return found variable: integer
     */
    protected Integer getVariable(String name) {
    	return this.localSymbols.getSymbol(name);    
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
